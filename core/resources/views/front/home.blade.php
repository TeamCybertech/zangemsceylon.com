@extends('layouts.front.master')
@section('css')
  <link rel="stylesheet" href="{{asset('assets/back/vendor/ladda/dist/ladda-themeless.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/back/vendor/sweetalert/lib/sweet-alert.css')}}">
@endsection

@section('content')
<!-- Header-->
<header class="intro">
  <!-- Intro Header-->
  <div class="intro-body">
    <!-- <h4>Smart <span class="bold">Business</span> Template
        </h4> -->
    <h1>Zan Gems &#x26; Jewellery Ceylon
      <!-- <span class="badge hidden-sm hidden-xs">html</span> --></h1>
    <div data-wow-delay="1s" class="scroll-btn wow fadeInDown"><a href="#about" class="page-scroll"><span class="mouse"><span class="weel"><span></span></span></span></a></div>
  </div>
</header>
<!-- Slider-->
<section id="about" class="section-small">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <h3>— Welcome to Zan Gems & Jewellery Ceylon</h3>
        <h4>About us</h4>
        <p class="no-pad">Our Business is approved by Government under BR No 5G/7395 in Year 2007. (Government Approved Certificate No 12055)
          Every Gems &#x26; Jewelries we issue a Certificate approved by the Gem Cooperation. </p>
        <p class="no-pad">We have all kinds of Gem Stones, Pressure Stones, Semi Pressure Stones 100 0/0 Sterling Silver and Gold Jewelries.</p>
        <p></p>
        <h4>Our goal is total customer satisfaction and give a good value for our clients</h4>
      </div>
      <div data-wow-duration="2s" data-wow-delay=".2s" class="col-lg-5 col-lg-offset-1 wow zoomIn">
        <div id="carousel-light2" class="carousel slide carousel-fade">
          <!-- <ol class="carousel-indicators">
                <li data-target="#carousel-light2" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-light2" data-slide-to="1"></li>
                <li data-target="#carousel-light2" data-slide-to="2"></li>
              </ol> -->
          <div role="listbox" class="carousel-inner">
            <div class="item active"><img src="{{asset('assets/front/img/about/WhatsApp-Image-2018-02-12-at-10.15.02-PM.jpeg')}}" alt="" class="img-responsive center-block"></div>
            <!--       <div class="item"><img src="img/misc/5.png" alt="" class="img-responsive center-block"></div>
                <div class="item"><img src="img/misc/4.png" alt="" class="img-responsive center-block"></div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Portfolio-->
<section id="portfolio" class="bg-gray no-pad-btm">
  <div class="container text-center">
    <div class="row">
      <div class="col-sm-10 col-sm-offset-1">
        <h3>Our Gallery</h3>
        <ul class="portfolio-sorting list-inline text-center">
            <li><a href="portfolio-single.html" data-group="all" class="active">All</a></li>
          @foreach ($categories as $item)
            <li><a href="portfolio-single.html" data-group="{{$item->id }}">{{ $item->name}}</a></li>              
          @endforeach
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div id="grid" class="row portfolio-items">
      @foreach ($gallery as $item)
      <div data-groups="[{{$item->gallery_category_id}}, &quot;all&quot;]" class="col-md-3 col-sm-6 no-pad">
        <div class="portfolio-item"><a href="#"><img src="{{asset($item->path."/".$item->filename)}}" alt="">
                      <div class="portfolio-overlay">
                        <div class="caption">
                          <h5>{{$item->album_name}}</h5>
                        </div>
                      </div></a></div>
      </div>
      @endforeach
    </div>
  </div>
</section>
<div class="section-small action bg-gray text-center"><a href="portfolio-masonry-4.html" class="btn btn-dark-border">View All Portfolio</a></div>

<!-- Client Reviews-->
<section id="testimonials" class="quote section-small bg-img3 text-center">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <h3>WRITE A QUOTE</h3>
        <!-- Contact Form - Enter your email address on line 17 of the mail/contact_me.php file to make this form work. For more information on how to do this please visit the Docs!-->
        <form id="contactForm" name="sentMessage" novalidate="">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="name" class="sr-only control-label">You Name</label>
              <input id="name" type="text" placeholder="You Name" required="" data-validation-required-message="Please enter name" class="form-control input-lg">
              <span
                class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="email" class="sr-only control-label">You Email</label>
              <input id="email" type="email" placeholder="You Email" required="" data-validation-required-message="Please enter email"
                class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="message" class="sr-only control-label">Message</label>
              <textarea id="message" rows="2" placeholder="Message" required="" data-validation-required-message="Please enter a message."
                aria-invalid="false" class="form-control input-lg"></textarea><span class="help-block text-danger"></span>
            </div>
          </div>
          <div id="success"></div>
          <button type="submit" class="btn btn-white btn-sm">Send</button>
        </form>
      </div>
      <div class="col-md-6 col-md-offset-1">
        <!--  <h3 class="text-center">Client Reviews</h3> -->
        <div id="carousel-testimonials" data-ride="carousel" class="carousel slide carousel-fade">
          <!-- Indicators-->
          <ol class="carousel-indicators">
            <li data-target="#carousel-testimonials" data-slide-to="0" class="active"></li>
            @for ($i = 1; $i < count($reviews) - 1; $i++)
                <li data-target="#carousel-testimonials" data-slide-to="{{$i}}"></li>
            @endfor

          </ol>
          <!-- Wrapper for slides-->
          <div role="listbox" class="carousel-inner">
            @foreach ($reviews as $key => $item)
                <div class="item {{$key == 0 ? 'active' : ''}}"><img src="{{asset('assets/front/img/profile.jpg')}}" alt="" class="center-block">
                  <div class="carousel-caption">
                    <h2 class="classic">{{$item->name}}</h2>
                    <h5 class="no-pad">{{$item->message}}</h5>
                  </div>
                </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Contact Section-->
<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h3>— Interested? Let’s talk...</h3>
        <h5 class="no-pad">Zan Gems & Jewellery Ceylon
        </h5>
        <p><i class="fa fa-map-marker fa-fw fa-lg"></i> No 7, New Lane 2, Fort, Galle, Sri Lanka</p>

        <h5><i class="fa fa-envelope fa-fw fa-lg"></i> <a href="mailto:zangems786@gmail.com">zangems786@gmail.com</a>
        </h5>
        <h5><i class="fa fa-phone fa-fw fa-lg"></i> <a href="tel:+94771615135">+94 77 1615135</a>/<a href="tel:+94772382897">+94 77 2382897</a>
        </h5>
        <div class="row">
        <div class="col-sm-2"><img src="{{asset('assets/front/img/TA_brand_logo.png')}}" style="width: 150px;" /></div>
          <div class="col-sm-2 col-md-offset-2"><img src="{{asset('assets/front/img/Background.png')}}" style="width: 150px;" /></div>
        </div>

      </div>
      <div class="col-md-5 col-md-offset-2">
        <h3>SAY HELLO</h3>
        <!-- Contact Form - Enter your email address on line 17 of the mail/contact_me.php file to make this form work. For more information on how to do this please visit the Docs!-->
        <form name="sentMessage" id="mailForm" novalidate="">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="name" class="sr-only control-label">Your Name</label>
              <input id="name" type="text" placeholder="Your Name" required="true" data-validation-required-message="Please enter name"
                class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="email" class="sr-only control-label">Your Email</label>
              <input id="email" type="email" placeholder="Your Email" required="true" data-validation-required-message="Please enter email"
                class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="subject" class="sr-only control-label">Subject</label>
              <input id="subject" type="text" placeholder="Subject" required="" data-validation-required-message="Please enter subject"
                class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="message" class="sr-only control-label">Message</label>
              <textarea id="message" rows="2" placeholder="Message" required="true" data-validation-required-message="Please enter a message."
                aria-invalid="false" class="form-control input-lg"></textarea><span class="help-block text-danger"></span>
            </div>
          </div>
          <div id="success"></div>
          <button type="submit" class="btn btn-dark">Send</button>
        </form>
      </div>
    </div>
  </div>
</section>
<!-- Map Section-->
<div id="map"></div>
@endsection
@section('js')
    <script src="{{asset('assets/back/vendor/ladda/dist/ladda.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/sweetalert/lib/sweet-alert.min.js')}}"></script>
    <script src="{{asset('assets/front/js/review.js')}}"></script>
@endsection