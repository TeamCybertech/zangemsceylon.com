@extends('layouts.front.master')
@section('content')
<header data-background="{{asset('assets/front/img/header/10.jpg')}}" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>Management Team</h1>
        <h4><a href="{{ url('/') }}">Home</a> / <a href="{{ url('/about-us') }}">About Us</a> / Management Team</h4>
      </div>
    </header>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <h5>Mr. Lalit Godamunne – Chairman</h5>
            <h4>B. A (Econ. Hons) University of Ceylon Diploma, Business Management IMEDE SWiz</h4>
            <p>Mr. Godamunne an international civil servant and the immediate past President of the Sri Lanka Coconut Grower`s Association. He has extensive experience in management and advisory services.</p>
            <p>He has previously held Chairmanships and Directorships in the public and international co-operations including Country Director to UN World Food Programme, Advisor to State Oils and Fats Corporation, Secretary General/Board Director to Mahaweli Authority of Sri Lanka, Chairman to Mahaweli Venture Capital Company Ltd., Sri Lanka-China joint venture Indigenous Medicine Hospitals Ltd and the State Hardware Corporation. He has also served on the Board of Mahaweli Marine Cement Ltd. and National Institute of Business Management and as a non-executive director of Hayleys PLC, Haycarbs and Hayleys Exports PLC. Currently he is a non-executive at Hayleys Agriculture Holdings and Board of Director of Coconut Research Institute.</p>
          </div>
          <div class="col-md-4">  
          <p><img src="{{asset('assets/front/img/team/lalit.jpg')}}" alt="" class="img-responsive center-block"></p>
            <ul class="list-inline text-center">
              <li><a href="http://twitter.com/"><i class="fa fa-twitter fa-lg"></i></a></li>
              <li><a href="http://facebook.com/"><i class="fa fa-facebook fa-lg"></i></a></li>
              <li><a href="http://googleplus.com/"><i class="fa fa-google-plus fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      <hr/>
        <div class="row">
          <div class="col-md-8">
            <h5>Mr. Nanda Abeywickrama – Director Natural Resources And Environment</h5>
            <h4>BA (Social Sciences) University of Ceylon PGDip in Development Studies Cambridge University, UK Cert in Agricultural Development Planning FAO, Rome</h4>
            <p>Mr. Abeywickrama an international civil servant, until recently a Special Advisor to the DG International Water Management Institute (IWMI). As Secretary to the Ministry of Land and Land Development and as Director International Cooperation IWMI. Mr. Abeywickrama has acquired in depth knowledge and experience at national and international levels in the fields of natural resources management and environmental protection. He has also served as a policy and institutional reform advisor for UNFAO, World Bank, ADB, JICA and USAID. Concurrently he has served as Chairman of Employees Trust Fund Board, Chair of GWP South Asia Chapter and Vice Chair of IIMI Board of Governors.</p>
          </div>
          <div class="col-md-4">  
          <p><img src="{{asset('assets/front/img/team/t2.jpg')}}" alt="" class="img-responsive center-block"></p>
            <ul class="list-inline text-center">
              <li><a href="http://twitter.com/"><i class="fa fa-twitter fa-lg"></i></a></li>
              <li><a href="http://facebook.com/"><i class="fa fa-facebook fa-lg"></i></a></li>
              <li><a href="http://googleplus.com/"><i class="fa fa-google-plus fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      <hr/>
        <div class="row">
          <div class="col-md-8">
            <h5>Dr. Sarath Abayawardana – Director R&D / International Development</h5>
            <h4>Ph.D in Chemical Engineering, University of London B.Sc, (Eng) First Class Hon. Mechanical Engineering, University of Ceylon. Fellow, Institution of Chemical Engineers, London Fellow, Institution of Engineers, Sri Lanka</h4>
            <p>Dr. Sarath Abayawardana has over 30 years` of experience in science and engineering sectors and currently functions as a Director on the Haycab PLC Board as well.</p>
            <p>He was the former Program Director at the ‘Coordinating Secretariat for Science Technology and Innovation’ and has worked with major national and international organizations. He at various times was the Director/CEO of the National Science Foundation; Technical Director, Unilever Sri Lanka; Product Engineer, Unilever plc London; Consultant to ADB vocational training projects; and also was an international researcher at the International Water Management Institute.</p>
            <p>He has also served as a member on numerous Governing Boards and high-level Committees, both in the public and private sectors.</p>
          </div>
          <div class="col-md-4">  
          <p><img src="{{asset('assets/front/img/team/t3.jpg')}}" alt="" class="img-responsive center-block"></p>
            <ul class="list-inline text-center">
              <li><a href="http://twitter.com/"><i class="fa fa-twitter fa-lg"></i></a></li>
              <li><a href="http://facebook.com/"><i class="fa fa-facebook fa-lg"></i></a></li>
              <li><a href="http://googleplus.com/"><i class="fa fa-google-plus fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      <hr/>
        <div class="row">
          <div class="col-md-8">
            <h5>Mrs. S.L. Kuruppu – Director</h5>
            <h4>B.A (Hons) Econ, Uniersity of Peradeniya</h4>
            <p>Mrs. Kuruppu has nearly 35 years` of experience in the public sector in Sri Lanka.</p>
            <p>She started her career as a Research Officer at the Ministry of Planning and Economic Affairs and subsequently retired as the Director General of External Resources.</p>
            <p>She has extensive experience in dealing with donor agencies and in multi-lateral and bilateral aid negotiations on behalf of the government of Sri Lanka. She is currently involved in range of social and community development activities.</p>
          </div>
          <div class="col-md-4">  
          <p><img src="{{asset('assets/front/img/team/t4.jpg')}}" alt="" class="img-responsive center-block"></p>
            <ul class="list-inline text-center">
              <li><a href="http://twitter.com/"><i class="fa fa-twitter fa-lg"></i></a></li>
              <li><a href="http://facebook.com/"><i class="fa fa-facebook fa-lg"></i></a></li>
              <li><a href="http://googleplus.com/"><i class="fa fa-google-plus fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      <hr/>
        <div class="row">
          <div class="col-md-8">
            <h5>Mr. Keerthi De Silva – Director Finance – FCA, FCMA</h5>
            <h4>Fellow Member of the Institute of Chartered Accountants of Sri Lanka Fellow Member of the Institute of Certified Management Accountants of Sri Lanka Member of Institute of Directors</h4>
            <p>Mr. Keerthi De Silva has extensive years of experience in finance and business management services. He started his career as a Finance Manager and later to Finance Director in many private and public sector organizations.</p>
            <p>He is currently the Managing Director of Infotechs (Pvt) Ltd & also serves on the Boards of Infotechs International (Pvt) Ltd., WaveNet International (Pvt) Ltd and Infotechs Travels (Pvt) Ltd.</p>
          </div>
          <div class="col-md-4">  
          <p><img src="{{asset('assets/front/img/team/t5.jpg')}}" alt="" class="img-responsive center-block"></p>
            <ul class="list-inline text-center">
              <li><a href="http://twitter.com/"><i class="fa fa-twitter fa-lg"></i></a></li>
              <li><a href="http://facebook.com/"><i class="fa fa-facebook fa-lg"></i></a></li>
              <li><a href="http://googleplus.com/"><i class="fa fa-google-plus fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      <hr/>
        <div class="row">
          <div class="col-md-8">
            <h5>Mr. Susantha Pinto – Director IT & Education</h5>
            <h4>Professional Accountant Bachelor of Philosophy Member of the Institute of Directors</h4>
            <p>Mr. Pinto has diverse experience over 30 years having started his career as an Accountant at Capital Maharaja Organization which is a leading Private sector company in Sri Lanka. He further served as a Director to Bartleet Freighters Ltd. and Bartleet Electronics Ltd. and was formerly the Director of American Chamber of Commerce. Currently he serves as the Chairman to Infotechs (Pvt) Ltd. and serves on the Boards of Infotechs International (Pvt) Ltd., WaveNet International (Pvt) Ltd., and Infotechs Travels (Pvt) Ltd.</p>
          </div>
          <div class="col-md-4">  
          <p><img src="{{asset('assets/front/img/team/t6.jpg')}}" alt="" class="img-responsive center-block"></p>
            <ul class="list-inline text-center">
              <li><a href="http://twitter.com/"><i class="fa fa-twitter fa-lg"></i></a></li>
              <li><a href="http://facebook.com/"><i class="fa fa-facebook fa-lg"></i></a></li>
              <li><a href="http://googleplus.com/"><i class="fa fa-google-plus fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      <hr/>
        <div class="row">
          <div class="col-md-8">
            <h5>Eng. P.W.C. Dayaratne – General Manager Engineering</h5>
            <h4>B. Sc (Engineering) Civil, University of Ceylon, 1969. Post-Graduate Diploma in Irrigation Engineering, Catholic University, Leuven, Belgium MSc (Engineering) Concrete Technology, University of New South Wales, Australia</h4>
            <p>Eng. Dayaratne has over 45 years of experience in the engineering field having worked in the Department of Irrigation at the capacity of Senior Irrigation Engineer, Chief Resident Engineer and Deputy Director. He was also the Deputy Director to Training and Research Centre at the Department of Irrigation. Prior to joining Infotechs-IDEAS he was a Freelance Consultant in Water Resources Sector in Sri Lanka.</p>
          </div>
          <div class="col-md-4">  
          <p><img src="{{asset('assets/front/img/team/t7.jpg')}}" alt="" class="img-responsive center-block"></p>
            <ul class="list-inline text-center">
              <li><a href="http://twitter.com/"><i class="fa fa-twitter fa-lg"></i></a></li>
              <li><a href="http://facebook.com/"><i class="fa fa-facebook fa-lg"></i></a></li>
              <li><a href="http://googleplus.com/"><i class="fa fa-google-plus fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      <hr/>
        <div class="row">
          <div class="col-md-8">
            <h5>Mr. Tissa Warnasuriya – General Manager - Corporate Affairs</h5>
            <h4>Bachelor of Science (B. Sc.) degree from University of Kelaniya and a Masters Degree in Public Administration (MPA) and Postgraduate Diploma in Provincial Administration (PGD) from University of Sri Jayewardenepura.</h4>
            <p>Mr Tissa Warnasuriya, has about 34 years of public service at Divisional, Provincial, National and International Agencies. While in government, has served a variety of institutions and industries, gaining experiences in development, action research & policy making. While elevating from the position of Assistant Government Agent (1977) to a Ministry Secretary (2004), he served as an Institutional Development & Resources Management Specialist at International Water Management Institute (IWMI (1993), Director General of Tourism (1998), Governing Board Member of IWMI 2004), Chairman of Council of Agriculture Research Policy (CARP) and Chairman of Hector Kobbekaduwa Research & Training Institute (HARTI (2004- 2007). He, also has executed multitude of development projects funded by USAID, JBIC, WB, ADB, JICA, FAO & IFAD in his capacity as the Secretary of Agriculture.</p>
          </div>
          <div class="col-md-4">  
          <p><img src="{{asset('assets/front/img/team/t8.jpg')}}" alt="" class="img-responsive center-block"></p>
            <ul class="list-inline text-center">
              <li><a href="http://twitter.com/"><i class="fa fa-twitter fa-lg"></i></a></li>
              <li><a href="http://facebook.com/"><i class="fa fa-facebook fa-lg"></i></a></li>
              <li><a href="http://googleplus.com/"><i class="fa fa-google-plus fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      <hr/>
        <div class="row">
          <div class="col-md-8">
            <h5>Ms. Manisha Kulasekere – Deputy General Manager</h5>
            <h4>BSc in Teaching Technology, Sikkim Manipal University, India. Certificate in Management Consultancy, Institute of Consultants, UK Member of Chartered Institute of Consultants Certificate in training on e-Library Learning by Queen’s University, Kingston, Canada. Diploma in Secretarial Services, Ladies College, Department of Vocational Studies, Colombo.</h4>
            <p>Ms. Kulasekere started her career as a Secretary to Bartleet Mecklai & Roy Money Brokers and has immense experience in the field of project coordination and management both locally and internationally. Some of her main involvements of are with World Bank funded Undergraduate Education Project, Governance of the Social Development/Care Centres in Post-Tsunami Sri Lanka Project executed by Queen’s University ICACBR, Canada and Project of International Law on Landmines & Explosive Remnants of War collaboration with government, UNICEF, CTF & Sarvodaya, SME Development programme funded by Scottish Government , Mid-Term Review of EU funded projects, SEMP II Impact Evaluation study with Ministry of Education.</p>
          </div>
          <div class="col-md-4">  
          <p><img src="{{asset('assets/front/img/team/t9.jpg')}}" alt="" class="img-responsive center-block"></p>
            <ul class="list-inline text-center">
              <li><a href="http://twitter.com/"><i class="fa fa-twitter fa-lg"></i></a></li>
              <li><a href="http://facebook.com/"><i class="fa fa-facebook fa-lg"></i></a></li>
              <li><a href="http://googleplus.com/"><i class="fa fa-google-plus fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      <!-- <hr/>
        <div class="row">
          <div class="col-md-8">
            <h5></h5>
            <h4></h4>
            <p></p>
          </div>
          <div class="col-md-4">  
          <p><img src="{{asset('assets/front/img/team/t10.jpg')}}" alt="" class="img-responsive center-block"></p>
            <ul class="list-inline text-center">
              <li><a href="http://twitter.com/"><i class="fa fa-twitter fa-lg"></i></a></li>
              <li><a href="http://facebook.com/"><i class="fa fa-facebook fa-lg"></i></a></li>
              <li><a href="http://googleplus.com/"><i class="fa fa-google-plus fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      <hr/> -->
      </div>
    </section>
@endsection