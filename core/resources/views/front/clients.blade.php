@extends('layouts.front.master')
@section('content')
<header data-background="{{asset('assets/front/img/header/10.jpg')}}" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>Clients &#x26; Associations</h1>
        <h4><a href="{{ url('/') }}">Home</a> / <a href="{{ url('/about-us') }}">About Us</a> / Clients &#x26; Associations</h4>
      </div>
    </header>
    <!-- Clients Block-->
    <section id="clients" class="bg-gray">
      <div class="container text-center">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <h3>Our Clients</h3>        
          </div>
        </div>
        <div class="row grid-pad">
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/1.jpg')}}" alt="" class="img-responsive center-block">
           
           
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/2.jpg')}}" alt="" class="img-responsive center-block">
          
            
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/3.jpg')}}" alt="" class="img-responsive center-block">
           
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/4.jpg')}}" alt="" class="img-responsive center-block">
          
            
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/5.jpg')}}" alt="" class="img-responsive center-block">
          
            
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/6.jpg')}}" alt="" class="img-responsive center-block">
          </div>

          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/7.jpg')}}" alt="" class="img-responsive center-block">
          </div>

          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/8.jpg')}}" alt="" class="img-responsive center-block">
          </div>

          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/9.jpg')}}" alt="" class="img-responsive center-block">
          </div>

          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/10.jpg')}}" alt="" class="img-responsive center-block">
          </div>

          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/11.jpg')}}" alt="" class="img-responsive center-block">
          </div>

          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/12.jpg')}}" alt="" class="img-responsive center-block">
          </div>

          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/13.jpg')}}" alt="" class="img-responsive center-block">
          </div>

          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/14.jpg')}}" alt="" class="img-responsive center-block">
          </div>

          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/clients/15.jpg')}}" alt="" class="img-responsive center-block">
          </div>

        </div>
      </div>

      <div class="container text-center">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <h3>Our Associations</h3>        
          </div>
        </div>
        <div class="row grid-pad">
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/associations/1.jpg')}}" alt="" class="img-responsive center-block">
           
           
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/associations/2.jpg')}}" alt="" class="img-responsive center-block">
          
            
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/associations/3.jpg')}}" alt="" class="img-responsive center-block">
           
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/associations/4.jpg')}}" alt="" class="img-responsive center-block">
          
            
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/associations/5.jpg')}}" alt="" class="img-responsive center-block">
          
            
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/associations/6.jpg')}}" alt="" class="img-responsive center-block">
          </div>

          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/associations/7.jpg')}}" alt="" class="img-responsive center-block">
           
           
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/associations/8.jpg')}}" alt="" class="img-responsive center-block">
          
            
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/associations/9.jpg')}}" alt="" class="img-responsive center-block">
           
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/associations/10.jpg')}}" alt="" class="img-responsive center-block">
          
            
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/associations/11.jpg')}}" alt="" class="img-responsive center-block">
          
            
          </div>
          <div class="col-sm-6 col-md-4"><img src="{{asset('assets/front/img/about/associations/12.jpg')}}" alt="" class="img-responsive center-block">
          </div>

        </div>
      </div>
    </section>
@endsection