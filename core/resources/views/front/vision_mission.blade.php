@extends('layouts.front.master')
@section('content')
<header data-background="{{asset('assets/front/img/header/10.jpg')}}" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>Vision &#x26; Mission</h1>
        <h4><a href="{{ url('/') }}">Home</a> / <a href="{{ url('/about-us') }}">About Us</a> / Vision &#x26; Mission</h4>
      </div>
    </header>
    <!-- Slider-->
    <section id="action-slider">
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h3>Vision</h3>
            <p>To be Sri Lanka`s leading Consultancy firm for client value creation through proficiency, innovation, integrity & sustainability.</p>

            <h3>Mission</h3>
            <p>Provision of high quality consultancy and development solutions by engaging high caliber professionals and experts supported by well-trained in-house management staff.</p>
          </div>
          <div class="col-lg-5">
            <div id="carousel-light" class="carousel slide carousel-fade">
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="{{asset('assets/front/img/about/vision-mission.jpg')}}" alt="" class="img-responsive center-block"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection