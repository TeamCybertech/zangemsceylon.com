@extends('layouts.front.master')
@section('content')
    <header data-background="{{asset('assets/front/img/header/5.jpg')}}" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <div class="container">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <h4>Home / Latest news / {{$news->title}}</h4>
            <h2>{{ $news->title }}</h2>
            </div>
          </div>
        </div>
      </div>
    </header>
    <section id="news-single" class="section-small">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-12">
                <h4>{{ $news->title }}</h4>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div id="carousel-news" class="carousel slide carousel-fade">
                  <ol class="carousel-indicators indicators-inside">
                    <li data-target="#carousel-news" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-news" data-slide-to="1"></li>
                    <li data-target="#carousel-news" data-slide-to="2"></li>
                  </ol>
                  <div class="carousel-inner">
                    @foreach ($news->newsImages as $key => $item)
                        <div class="item {{$key == 1 ? 'active' : ''}}"><img src="{{asset($item->src)}}" alt="" class="img-responsive center-block"></div>
                    @endforeach
                    
                    {{-- <div class="item"><img src="{{asset('assets/front/img/portfolio/9.jpg')}}" alt="" class="img-responsive center-block"></div>
                    <div class="item"><img src="{{asset('assets/front/img/portfolio/4.jpg')}}" alt="" class="img-responsive center-block"></div> --}}
                  </div><a href="#carousel-news" data-slide="prev" class="left carousel-control"><span class="icon-prev"></span></a><a href="#carousel-news" data-slide="next" class="right carousel-control"><span class="icon-next"></span></a>
                </div>
              </div>
              <div class="col-md-6">
                <p><img src="{{asset($news->image)}}" alt="" class="img-responsive center-block"></p>
                <!-- p.small Collaboratively administrate empowered markets  via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI. Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain-->
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                
                <p>{{ $news->description }}</p>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section-small bg-white">
      <div class="container grid-pad">
        <h3>Recent News</h3>
        <div class="row">
          @foreach ($recent as $item)
              <div class="col-sm-6 col-md-3"><a href="{{ url('/news/'.$item->slug) }}"><img src="{{asset($item->image)}}" alt="" class="img-responsive center-block"/>
                            <h5>{{$item->title}}</h5></a>
              <p>{{substr($item->description,0, 100)}}</p>
              </div>
          @endforeach
          
          
        </div>
      </div>
    </section>
<!--     <div class="section section-small">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <nav>
              <ul class="pager">
                <li class="previous"><a href="portfolio-single.html" class="text-muted"><span aria-hidden="true"></span><i class="fa fa-angle-left"></i> PREVIOUS</a></li>
                <li><a href="portfolio-masonry-4.html"><i class="fa ion-grid fa-2x"></i></a></li>
                <li class="next"><a href="portfolio-single.html" class="text-muted">NEXT <span aria-hidden="true"></span><i class="fa fa-angle-right"></i></a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div> -->
@endsection