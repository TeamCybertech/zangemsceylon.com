@extends('layouts.front.master')
@section('content')
<header data-background="{{asset('assets/front/img/header/10.jpg')}}" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>Careers</h1>
        <h4><a href="{{ url('/') }}">Home</a> / Careers</h4>
      </div>
    </header>
    <!-- Slider-->
    <section id="action-slider">
      <div class="container">
        <div class="row text-center">
          <div class="col-lg-10 col-lg-offset-1">
            <h3>Registration Of National And International Consultants</h3>            
          </div>
        </div>
        <div class="row">
          <div class="col-lg-7">
            <h3>Sectors Applicable:</h3>
            <h4>Project Management</h4>
            <p class="no-pad">Financial Specialists</p>
            <p class="no-pad">Economists</p>
            <p></p>

            <h4>Project Impact Evaluation Specialists</h4>
            <p class="no-pad">Social Development Sector</p>
            <p class="no-pad">Sociologistt</p>
            <p class="no-pad">Resettlement Expert</p>
            <p class="no-pad">Gender Specialists</p>
            <p class="no-pad">Community Development Specialists</p>
            <p></p>

            <h4>All sub sectors in Environmental Sector Development</h4>

            <h4>Specialists of all sub sectors in Road Development</h4>

            <h4>Specialists in all sub sectors in Water Resources Development</h4>

            <h4>Other</h4>

            <p class="no-pad">Education &#x26; Training (Primary/Secondary/Skills & Vocational)</p>
            <p class="no-pad">SME Development/ Micro finance &#x26; Capacity Development/Livelihood</p>
            <p class="no-pad">IT &#x26; Data Management Specialists</p>
            <p class="no-pad">Food Safety Experts</p>
            <p></p>

            <h4>Qualification, Work Experience and Skills required:</h4>

            <p class="no-pad">Minimum of Bachelor`s degree relevant to the sector for the position</p>
            <p class="no-pad">Minimum of 5 years` work experience (Project/Consultancy based work)</p>
            <p class="no-pad">Good standard of both written and verbal skills in English and Sinhala or Tamil</p>
            <p class="no-pad">Should be willing to work in teams</p>
            <p></p>

            <h4>CV Format:</h4>
            <ol>
              <li>Name of Consultant</li>
              <li>Complete Personal Contact Details</li>
              <li>Educational &#x26; Professional Experience</li>
              <li>Other Training</li>
              <li>Countries of Work Experience</li>
              <li>Languages</li>
              <li>Employment Records
                <ul>
                   <li>Name of the assignment or project,</li>
                   <li>Year: From dd/mm/yyyy To dd/mm/yyyy</li>
                   <li>Location</li>
                   <li>Client</li>
                   <li>Main Project Features</li>
                   <li>Position(s) held</li>
                   <li>Activities performed</li>
                </ul>
              </li>
              <li>Names and the contact details of no – related referees</li>
            </ol>

            <h4>If you possess the above qualifications, please submit your CV to <a href="mailto:mail@infotechs-ideas.com">mail@infotechs-ideas.com</a> mentioning the sector/s applying in the subject line of the email.</h4>
     

          </div>
          <div class="col-lg-5">
            <div id="carousel-light" class="carousel slide carousel-fade">
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="{{asset('assets/front/img/careers/careers.jpg')}}" alt="" class="img-responsive center-block"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection