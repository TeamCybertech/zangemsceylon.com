@extends('layouts.front.master')
@section('content')
<!-- Header-->
    <header data-background="{{asset('assets/front/img/header/11.jpg')}}" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>Contact Us</h1>
        <h4><a href="{{ url('/') }}">Home</a> / Contact Us</h4>
      </div>
    </header>
    <!-- Contact Section-->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <h3>contact us</h3>
            <p>Feel free to contact us. A business has to be involving, it has to be fun, and it has to exercise your creative instincts. Start where you are. Use what you have. Do what you can. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            <hr>
            <h5><i class="fa fa-map-marker fa-fw fa-lg"></i> Address: <br/>
              546/6, Galle Road, Colombo 03, Sri Lanka.
            </h5>
            <h5><i class="fa fa-envelope fa-fw fa-lg"></i> Support: <br/>
              <a href="mailto:Mail@Infotechs-Ideas.com">Mail@Infotechs-Ideas.com</a><br/><br/>
              <span class="bold">General Manager Engineering –</span><br/>
              <a href="mailto:Dayaratne@Infotechs-Ideas.com">Dayaratne@Infotechs-Ideas.com</a>
            </h5>
            <h5><i class="fa fa-phone fa-fw fa-lg"></i> Phone: <br/>
              <a href="tel:+94112370755">+94 11 2370755</a><br/>
              <a href="tel:+94112370756">+94 11 2370756</a><br/>
              <a href="tel:+94112372104">+94 11 2372104</a>
            </h5>
          </div>
          <div class="col-md-5 col-md-offset-2">
            <h3>Get in Touch</h3>
            <!-- Contact Form - Enter your email address on line 17 of the mail/contact_me.php file to make this form work. For more information on how to do this please visit the Docs!-->
            <form id="contactForm" name="sentMessage" novalidate="">
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="name" class="sr-only control-label">You Name</label>
                  <input id="name" type="text" placeholder="You Name" required="" data-validation-required-message="Please enter name" class="form-control input-lg"><span class="help-block text-danger"></span>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="email" class="sr-only control-label">You Email</label>
                  <input id="email" type="email" placeholder="You Email" required="" data-validation-required-message="Please enter email" class="form-control input-lg"><span class="help-block text-danger"></span>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="phone" class="sr-only control-label">You Phone</label>
                  <input id="phone" type="tel" placeholder="You Phone" required="" data-validation-required-message="Please enter phone number" class="form-control input-lg"><span class="help-block text-danger"></span>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="message" class="sr-only control-label">Message</label>
                  <textarea id="message" rows="2" placeholder="Message" required="" data-validation-required-message="Please enter a message." aria-invalid="false" class="form-control input-lg"></textarea><span class="help-block text-danger"></span>
                </div>
              </div>
              <div id="success"></div>
              <button type="submit" class="btn btn-dark">Send</button>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- Map Section-->
    <div id="map"></div>

    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/-->
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyAzZ4aG6edNVf1xfPup8uB7DjdwXbokTCM"></script>
    <script src="{{asset('assets/front/js/map.js')}}"></script>
@endsection