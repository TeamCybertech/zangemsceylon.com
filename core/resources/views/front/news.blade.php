@extends('layouts.front.master')
@section('content')
<header data-background="{{asset('assets/front/img/header/10.jpg')}}" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>News</h1>
        <h4></h4>
      </div>
    </header>
    <!-- News Block-->
    <section id="news" class="section-small">
      <div class="container">
        <h3 class="pull-left">News</h3>
        <div class="pull-right">
          <h4>OUR LATEST NEWS</h4>
        </div>
        <div class="clearfix"></div>
        <div class="row grid-pad">

          @foreach ($news as $item)
              <div class="col-sm-6 col-md-3"><a href="{{ url('/news/'.$item->slug) }}"><img src="{{asset($item->image)}}" alt="" class="img-responsive center-block">
              <h5>{{$item->title}}</h5></a>
                <p>
                  {{ substr($item->description,0, 140) }}
                </p><a href="{{ url('/news/'.$item->slug) }}" class="btn btn-gray btn-xs">Read more</a>
              </div>
          @endforeach          
        </div>
      </div>
    </section>

    @include('front.pagination', ['paginator' => $news])
    
    <div class="section action section-small bg-gray2">
      <div class="container wow fadeIn">
        <div class="row">
          <div class="col-md-10">
            <h3 class="no-pad">Get your latest infotect news!</h3>
          </div>
          <div class="col-md-2 text-right"><a href="#" class="btn btn-dark wow fadeInDown">Subscribe Us</a></div>
        </div>
      </div>
    </div>
@endsection