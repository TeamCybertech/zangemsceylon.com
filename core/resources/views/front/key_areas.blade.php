@extends('layouts.front.master')
@section('content')
    <header data-background="{{asset('assets/front/img/header/10.jpg')}}" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>Key Areas Of Business</h1>
        <h4><a href="{{ url('/') }}">Home</a> / Key areas</h4>
      </div>
    </header>
    <section id="action-slider">
      <div class="container">
        <div class="row" id="et">
          <div class="col-lg-7">
            <h3>EDUCATION &#x26; TRAINING</h3>
            <p>Education and Training are increasingly important factors in the multi-faceted drive to enhance productivity, stimulate economic competitiveness and eliminate poverty.</p>
            <p>Our Expertise:</p>

            <ul style="list-style-type: none;">
              <li><i class="fa fa-caret-right"></i> Primary &amp; Secondary education</li>
              <li><i class="fa fa-caret-right"></i> Skills Development</li>
              <li><i class="fa fa-caret-right"></i> Vocational Training</li>
              <li><i class="fa fa-caret-right"></i> Training Needs Assessments</li>
              <li id="it"><i class="fa fa-caret-right"></i> Capacity Development</li>
              <li><i class="fa fa-caret-right"></i> Professional Development</li>
            </ul>

          </div>
          <div class="col-lg-5">
            <div id="carousel-light" class="carousel slide carousel-fade">
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="{{asset('assets/front/img/key-areas/training.jpg')}}" alt="" class="img-responsive center-block"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row" id="it">
          <div class="col-lg-7">
            <h3>ENGINEERING &#x26; IT</h3>
            <p>Provision of Professional Engineering &#x26; IT services and expertise to both public and private sector organizations. In this dynamic development environment, Infotechs IDEAS takes pride in providing expert panels of consultants with technical specialization and experience leading to successful project completion</p>
            <p>Our Expertise:</p>

            <ul style="list-style-type: none;">
              <li><i class="fa fa-caret-right"></i> Irrigation</li>
              <li><i class="fa fa-caret-right"></i> Water Supply &amp; Sanitation</li>
              <li><i class="fa fa-caret-right"></i> Water Management</li>
              <li><i class="fa fa-caret-right"></i> Construction Supervision</li>
              <li><i class="fa fa-caret-right"></i> Online CAD Drafting</li>
              <li><i class="fa fa-caret-right"></i> Application Development</li>
              <li id="natural"><i class="fa fa-caret-right"></i> Database Development</li>
              <li><i class="fa fa-caret-right"></i> Web Based Application Development</li>
            </ul>

          </div>
          <div class="col-lg-5">
            <div id="carousel-light" class="carousel slide carousel-fade">
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="{{asset('assets/front/img/key-areas/it.jpg')}}" alt="" class="img-responsive center-block"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row" id="nre">
          <div class="col-lg-7">
            <h3>NATURAL RESOURCES &#x26; ENVIRONMENT</h3>
            <p>As a country with abundant blessings from nature, one of our biggest challenges and responsibilities is the conservation and sustainable development of our natural resources and environment. We provide flexible, innovative solutions in congruence with local traditions and customs land consistent international norms.</p>
            <p>Our Expertise:</p>

            <ul style="list-style-type: none;">
              <li><i class="fa fa-caret-right"></i> Environment Impact Assessments</li>
              <li><i class="fa fa-caret-right"></i> Initial Environment Evaluations</li>
              <li><i class="fa fa-caret-right"></i> Environment Management Plans</li>
              <li><i class="fa fa-caret-right"></i> Wild-Life &amp; Bio Diversity</li>
              <li id="transport"><i class="fa fa-caret-right"></i> Natural Resources Management</li>
              <li><i class="fa fa-caret-right"></i> Climate Change</li>
            </ul>

          </div>
          <div class="col-lg-5">
            <div id="carousel-light" class="carousel slide carousel-fade">
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="{{asset('assets/front/img/key-areas/environment.jpg')}}" alt="" class="img-responsive center-block"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row" id="ti">
          <div class="col-lg-7">
            <h3>TRANSPORT &#x26; INFRASTRUCTURE</h3>
            <p>Safe, efficient and sustainable transport and infrastructure development is essential for supporting the socio-economic development of any country. We posses the competence to contribute to the prosperity of the economy by providing solutions for enhancing transport and infrastructure systems.</p>
            <p>Our Expertise:</p>

            <ul style="list-style-type: none;">
              <li><i class="fa fa-caret-right"></i> Pre – Project Surveys &amp; Assements</li>
              <li><i class="fa fa-caret-right"></i> Resettlement Plans</li>
              <li><i class="fa fa-caret-right"></i> Urban &amp; Rural Planning</li>
              <li><i class="fa fa-caret-right"></i> Design, Construction and Supervision</li>
            </ul>

          </div>
          <div class="col-lg-5">
            <div id="carousel-light" class="carousel slide carousel-fade">
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="{{asset('assets/front/img/key-areas/transport.jpg')}}" alt="" class="img-responsive center-block"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row" id="sed">
          <div class="col-lg-7">
            <h3>SOCIO – ECONOMIC DEVELOPMENT</h3>
            <p>Infotechs IDEAS contributes to the country`s goal of becoming a higher middle income country through socio-economic development studies and activities. The projects are countrywide, and the focus is frequently on regional and rural development, poverty alleviation, empowerment of the poor, travel and tourism, and industrial development.</p>
            <p>Our Expertise:</p>

            <ul style="list-style-type: none;">
              <li><i class="fa fa-caret-right"></i> SME Development</li>
              <li><i class="fa fa-caret-right"></i> Micro Finance</li>
              <li><i class="fa fa-caret-right"></i> Livelihoods</li>
              <li><i class="fa fa-caret-right"></i> Tourism Development</li>
              <li><i class="fa fa-caret-right"></i> Community Based Development</li>
              <li><i class="fa fa-caret-right"></i> Social Impact Assessment</li>
              <li id="project"><i class="fa fa-caret-right"></i> Social Management Frameworks</li>
              <li><i class="fa fa-caret-right"></i> Policy Review and Development</li>
            </ul>

          </div>
          <div class="col-lg-5">
            <div id="carousel-light" class="carousel slide carousel-fade">
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="{{asset('assets/front/img/key-areas/economic.jpg')}}" alt="" class="img-responsive center-block"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="row" id="pm">
          <div class="col-lg-7">
            <h3>PROJECT MANAGEMENT</h3>
            <p>We at Infotechs IDEAS have outstanding expertise in terms of knowledge, skills and techniques to execute projects efficiently and effectively.</p>
            <p>Our Expertise:</p>

            <ul style="list-style-type: none;">
              <li><i class="fa fa-caret-right"></i> Volunteer Transition</li>
              <li><i class="fa fa-caret-right"></i> Planning &amp; Design</li>
              <li><i class="fa fa-caret-right"></i> Strategic Planning</li>
              <li><i class="fa fa-caret-right"></i> Business Development Plans</li>
              <li><i class="fa fa-caret-right"></i> Mid-Term Reviews</li>
            </ul>

          </div>
          <div class="col-lg-5">
            <div id="carousel-light" class="carousel slide carousel-fade">
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="{{asset('assets/front/img/key-areas/pm.jpg')}}" alt="" class="img-responsive center-block"></div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section>
@endsection