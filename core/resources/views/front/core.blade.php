@extends('layouts.front.master')
@section('content')
<header data-background="{{asset('assets/front/img/header/10.jpg')}}" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>Core Values</h1>
        <h4><a href="{{ url('/') }}">Home</a> / <a href="{{ url('/about-us') }}">About Us</a> / Core Values</h4>
      </div>
    </header>
    <!-- Slider-->
    <section id="action-slider">
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h3>Core Values</h3>
            
            <ul style="list-style-type: none;">
              <li style="margin-bottom: 12px;"><i class="fa fa-caret-right"></i> Innovation</li>
              <li style="margin-bottom: 12px;"><i class="fa fa-caret-right"></i> Sustainability</li>
              <li style="margin-bottom: 12px;"><i class="fa fa-caret-right"></i> Inspirational Leadership</li>
              <li style="margin-bottom: 12px;"><i class="fa fa-caret-right"></i> Respect for individual</li>
              <li style="margin-bottom: 12px;"><i class="fa fa-caret-right"></i> Client value creation</li>
              <li style="margin-bottom: 12px;"><i class="fa fa-caret-right"></i> Best expert knowledge</li>
            </ul>

          </div>
          <div class="col-lg-5">
            <div id="carousel-light" class="carousel slide carousel-fade">
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="{{asset('assets/front/img/about/core-value.jpg')}}" alt="" class="img-responsive center-block"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection