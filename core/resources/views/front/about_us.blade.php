@extends('layouts.front.master')
@section('content')
<header data-background="{{asset('assets/front/img/header/10.jpg')}}" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>Company Overview</h1>
        <h4><a href="{{ url('/') }}">Home</a> / <a href="{{ url('/about-us') }}">About Us</a> / Company Overview</h4>
      </div>
    </header>
     <!-- Slider-->
    <section id="action-slider">
      <div class="container">
        <div class="row text-center">
          <div class="col-lg-8 col-lg-offset-2">
            <h3>Our Role Has Evolved Over The Years To Be:</h3>
            <p>“Providing high quality engineering and socio-economic consultancy services in planning, managing and implementation of development projects”</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-7">
            <h3>Welcome To Infotechs-Ideas.</h3>
            <p class="no-pad">We have achieved this by dedicating ourselves to discharge our responsibilities with the highest professional standards and ethics and delivering quality services on time.</p>
            <p class="no-pad">We are a multi-disciplinary development consultancy company based in Colombo, Sri Lanka. The Company was established in 1999 by a group of professionals who had reached the zenith of their careers serving in senior positions nationally and internationally. They continue to contribute their professional experience and add strength to all our activities. We are now one of Sri Lanka`s leading and much sought after companies in providing consulting and project management services in Sri Lanka, and South Asian and African countries. Over the years we have provided consultancy services to a portfolio of projects with an aggregated value of over USD 2 billion.</p>
            <p class="no-pad">Our strong performance and presence is the result of continued focused execution of our growth strategy.</p>
            <p class="no-pad"><span class="bold">Our goal is very clear.</span> We will continue to be a front-runner in our industry with our strategy of expanding the business by being proactive in developing networks, building partnerships and applying knowledge and innovation for sustainable solutions.</p>
            <p class="no-pad"><span class="bold">Our economic intent is also very clear and altruistic at the same time.</span> Substantial part of our profits are channeled for entrepreneur development activities and community development through our partner shareholder IDEAS (Initiatives in Development of Entrepreneur Approaches and Strategies), a not – for – profit organization.</p>
          </div>
          <div class="col-lg-5">
            <div id="carousel-light" class="carousel slide carousel-fade">
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="{{asset('assets/front/img/about/about.jpg')}}" alt="" class="img-responsive center-block"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection