<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Zangems Ceylon</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="pinterest" content="nohover"/>

     <!-- Bootstrap Core CSS-->
     <link href="{{asset('assets/front/css/bootstrap.min.css')}}" rel="stylesheet">
     <!-- Custom CSS-->
     <link href="{{asset('assets/front/css/universal.css')}}" rel="stylesheet">

    <!--  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> -->
    @yield('css')

    <script>
      var baseUrl = '{{url("/")}}';
    </script>

</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" class="top">
    <div id="preloader">
        <div id="status"></div>
    </div>

<!-- Navigation-->
    <nav class="navbar navbar-universal navbar-custom navbar-fixed-top navbar-onepage">
          <div class="container">
            <div class="navbar-header">
              <button type="button" data-toggle="collapse" data-target=".navbar-main-collapse" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
              <a href="#page-top" class="navbar-brand page-scroll">
                <!-- Text or Image logo--><img src="{{asset('assets/front/img/logo.png')}}" alt="Logo" class="logo"><img src="{{asset('assets/front/img/logodark.png')}}" alt="Logo" class="logodark"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling-->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
              <ul class="nav navbar-nav">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section-->
                <li class="hidden"><a href="#page-top"></a></li>
                <li><a href="index.html">Home</a></li>
                <li><a href="#about" class="page-scroll">About</a></li>
                <li><a href="#portfolio" class="page-scroll">Gallery</a></li>
                <li><a href="#contact" class="page-scroll">Contact</a></li>
              </ul>
            </div>
          </div>
        </nav>


    @yield('content')

    <!-- Footer Section-->
    <section class="section-small footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <h6>Copyright © 2018 <a href=""> Zan Gems &#x26; Jewellery Ceylon </a>
            </h6>
          </div>
          <div class="col-sm-3 col-sm-offset-1">
            <!--  <h6>We <i class="fa fa-heart fa-fw"></i> creative people
                </h6> -->
          </div>
          <div class="col-sm-3 col-sm-offset-1 text-right">
            <ul class="list-inline">
              <li><a href="http://forbetterweb.com/"><i class="fa fa-twitter fa-fw fa-lg"></i></a></li>
              <li><a href="http://forbetterweb.com/"><i class="fa fa-facebook fa-fw fa-lg"></i></a></li>
              <li><a href="http://forbetterweb.com/"><i class="fa fa-google-plus fa-fw fa-lg"></i></a></li>
              <li><a href="http://forbetterweb.com/"><i class="fa fa-linkedin fa-fw fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>

   <!-- jQuery-->
    <script src="{{asset('assets/front/js/jquery-1.12.4.min.js')}}"></script>
    <!-- Bootstrap Core JavaScript-->
    <script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
    <!-- Plugin JavaScript-->
    <script src="{{asset('assets/front/js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('assets/front/js/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('assets/front/js/device.min.js')}}"></script>
    <script src="{{asset('assets/front/js/form.min.js')}}"></script>
    <script src="{{asset('assets/front/js/form.contact.min.js')}}"></script>
    <script src="{{asset('assets/front/js/jquery.placeholder.min.js')}}"></script>
    <script src="{{asset('assets/front/js/jquery.shuffle.min.js')}}"></script>
    <script src="{{asset('assets/front/js/jquery.parallax.min.js')}}"></script>
    <script src="{{asset('assets/front/js/jquery.circle-progress.min.js')}}"></script>
    <script src="{{asset('assets/front/js/jquery.swipebox.min.js')}}"></script>
    <script src="{{asset('assets/front/js/smoothscroll.min.js')}}"></script>
    <script src="{{asset('assets/front/js/wow.min.js')}}"></script>
    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3E86i8mx1BZDlAaLcknh_mWl4F70i4os"></script>
    <script src="{{asset('assets/front/js/map.js')}}"></script>

    <script src="{{asset('assets/front/js/vegas/vegas.min.js')}}"></script>
    <script>
      $('body').vegas({
              delay: 9000,
              timer: false,
              transitionDuration: 2000,
              slides: [
                  {src: '{{asset("assets/front/img/header/main1.jpg")}}'},
                  {src: '{{asset("assets/front/img/header/22.jpg")}}'},
                  {src: '{{asset("assets/front/img/header/2.jpg")}}'}
              ],
              transition: 'fade',
              animation: 'kenburns'
          });
    </script>
    <!-- Custom Theme JavaScript-->
  <script src="{{asset('assets/front/js/universal.js')}}"></script>
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    @yield('js')
</body>
</html>




