<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * USER AUTHENTICATION MIDDLEWARE
 */

/*DONT USE THIS ROUTE FOR OTHER USAGE ----ONLY FOR THIS */
Route::group(['middleware' => ['auth']], function()
{
    Route::get('admin', [
      'as' => 'dashboard', 'uses' => 'WelcomeController@index'
    ]);   
	
});


Route::group([], function()
{	
    Route::get('/', [
      'as' => 'index', 'uses' => 'WebController@index'
    ]);  

    Route::get('/about-us', 'WebController@aboutUs');
    Route::get('/about-us/vision-mission', 'WebController@visionMission');
    Route::get('/about-us/core', 'WebController@core');
    Route::get('/about-us/team', 'WebController@team');
    Route::get('/about-us/clients', 'WebController@clients');
    Route::get('/key-areas', 'WebController@keyAreas');
    Route::get('/news', 'WebController@news');
    Route::get('/news/{id}', 'WebController@newsDetails');
    Route::get('/careers', 'WebController@careers');
    Route::get('/contact-us', 'WebController@contactUs');
    Route::get('/contact-us-submit', 'WebController@contactUsSubmit');
});


Route::get('/review', 'WebController@addReview');


/**
 * USER REGISTRATION & LOGIN
 */

Route::get('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@loginView'
]);
Route::post('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@login'
]);

Route::get('user/logout', [
  'as' => 'user.logout', 'uses' => 'AuthController@logout'
]);

Route::get('contact-us-submit', 'WebController@contactUsSubmit');