<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use GalleryManage\Models\GalleryCategory;
use GalleryManage\Models\Gallery;
use ReviewManage\Models\Review;
use Mail;

class WebController extends Controller {

	/*
		|--------------------------------------------------------------------------
		| Web Controller
		|--------------------------------------------------------------------------
		|
		| This controller renders the "marketing page" for the application and
		| is configured to only allow guests. Like most of the other sample
		| controllers, you are free to modify or remove it as you desire.
		|
	*/

    public function index() {
		$categories = GalleryCategory::get();
		$gallery = Gallery::get();
		$reviews = Review::whereApprove(1)->orderBy('id','desc')->limit(5)->get();
        return view('front.home',compact('categories','gallery','reviews'));
    }

	public function addReview(Request $request)
	{
		$this->validate($request,[
			'name' => 'required|max:255',
			'email' => 'required|email',
			'message' => 'required',
		]);

		Review::create($request->all());

		return response()->json('success', 200);
	}

	public function contactUsSubmit(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'msg' => 'required',
        ]);
        
        $inquiry = collect();

        $inquiry->name = $request->name;
        $inquiry->email = $request->email;
        $inquiry->msg = $request->msg;

        Mail::send('email.contact', ['inquiry' => $inquiry], function ($m) {
			$m->from('system@mail.com');
			$m->to('nuwan.nca@gmail.com')->subject('Inquiry');
		});
        
        return response()->json('success',200);
    }
}
