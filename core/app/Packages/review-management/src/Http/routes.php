<?php

Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/review', 'namespace' => 'ReviewManage\Http\Controllers'], function(){


      Route::get('list', [
        'as' => 'review.list', 'uses' => 'ReviewController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'review.list', 'uses' => 'ReviewController@jsonList'
      ]);

      Route::get('approve', [
        'as' => 'review.approve', 'uses' => 'ReviewController@approve'
      ]);



      Route::post('delete', [
        'as' => 'review.delete', 'uses' => 'ReviewController@delete'
      ]);


    });
});
