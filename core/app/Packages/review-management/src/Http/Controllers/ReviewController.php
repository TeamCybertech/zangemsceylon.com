<?php

namespace ReviewManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use ReviewManage\Models\Review;
use Sentinel;
use Image;
use \App\Feedback;

class ReviewController extends Controller {

    /**
     * View Branch List View
     *
     * @return Response
     */
    public function listView() {
        return view('ReviewManage::list');
    }

    /**
     * Device list
     *
     * @return Response
     */
    public function jsonList(Request $request) {
        if ($request->ajax()) {
            $data = Review::get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $review) {
                $approve = "<label class='label label-danger'>not approved</label>";

                if ($review->approve) {
                  $approve = "<label class='label label-success'>approved</label>";
                }

                $dd = array();
                array_push($dd, $i);
                array_push($dd, $review->name);
                array_push($dd,  $review->email);
                array_push($dd,  $review->message);
                array_push($dd,  $approve);
                array_push($dd,  '<center><a href="#" class="approve" data-type="review" data-id="' . $review->id . '" data-toggle="tooltip" data-placement="top" title="Approve / Disapprove"><i class="fa fa-check"></i></a></center>');


                $permissions = Permission::whereIn('name', ['review.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="product-delete" data-id="' . $review->id . '" data-toggle="tooltip" data-placement="top" title="Delete Blog"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }

            return response()->json(array('data' => $jsonList));
        } else {
            return response()->json(array('data' => []));
        }
    }


    /**
     * Delete a device
     * @param  Request $request branch id
     * @return Json           	json object with status of success or failure
     */
    public function delete(Request $request) {
      $this->validate($request,['id' => 'required|exists:reviews,id']);
      $id = $request->input('id');
      $blog = Review::find($id);
      $blog->delete();
      return response()->json(['status' => 'success']);
    }

    public function approve(Request $request)
    {
        $this->validate($request,['id' => 'required']);

        $id = $request->input('id');
        $type = $request->input('type');

        if ($type == "review") {
          $review = Review::find($id);
        }else {
          $review = Feedback::find($id);
        }

        $review->approve = !$review->approve;
        $review->save();
        return response()->json(['status' => 'success']);
    }


}
