<?php
namespace GalleryManage\Http\Controllers;

use App\Http\Controllers\Controller;
use GalleryManage\Http\Requests\GalleryRequest;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use GalleryManage\Models\GalleryCategory;

// use DeviceManage\Models\Device;
use Sentinel;
use Response;
use File;
use Validator;


class CategoryController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Gallery Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Show the GALLERY  add screen to the user.
	 *
	 * @return Response
	 */
	public function addView_category()
	{
		return view( 'GalleryManage::category.add');
	}

	/**
	 * Add new category CATEGORY data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add_category(Request $request)
	{
		$this->validate($request, ['name' => 'required']);

		GalleryCategory::create($request->all());

		return redirect('admin/gallery/category/add')->with(['success' => true,
				'success.message' => 'Category added',
				'success.title' => 'Well Done!']);
	}

	/**
	 * View GALLERY CATEGORY List View
	 *
	 * @return Response
	 */
	public function listView_category()
	{
		return view('GalleryManage::category.list');
	}

	/**
	 * category CATEGORY list
	 *
	 * @return Response
	 */
	public function jsonList_category(Request $request)
	{
		if($request->ajax()){
			$cats = GalleryCategory::get();
			$jsonList = array();
			$i=1;
			foreach ($cats as $key => $cat) {

				$dd = array();
				array_push($dd, $i);
       			array_push($dd, $cat->name);
				
				$permissions = Permission::whereIn('name',['gallery.category.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('admin/gallery/category/edit/'.$cat->id).'\'" data-toggle="tooltip" data-placement="top" title="Gallery "><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['gallery.category.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="category-delete" data-id="'.$cat->id.'" data-toggle="tooltip" data-placement="top" title="Delete Gallery"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}

	/**
	 * Delete a GALLERY CATEGORY
	 * @param  Request $request series id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete_category(Request $request)
	{
			$this->validate($request,['id' => 'required']);
			$id = $request->input('id');
			$cat = GalleryCategory::find($id);
      		$cat->delete();
			return response()->json(['status' => 'success']);

	}

	/**
	 * Show the GALLERY CATEGORY edit screen to the series.
	 *
	 * @return Response
	 */
	public function editView_category($id)
	{
		$galleryCategory= GalleryCategory::findOrFail($id);
		return view('GalleryManage::category.edit' )->with(['galleryCategory'=>$galleryCategory]);
	}

	/**
	 * Add new GALLERY CATEGORY data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit_category(GalleryRequest $request, $id)
	{
		$validate = Validator::make($request->all(), ['name' => 'name']);
		$galleryCategory = GalleryCategory::findOrFail($id);
		$galleryCategory->update($request->all());
	
		return redirect('admin/gallery/category/edit/'.$id)->with(['success' => true,
				'success.message' => 'Image update to the gallery',
				'success.title' => 'Well Done!']);
	}

}
